<?php

namespace Plugin\AsdMailChimp;

class Filter {
    public static function ipWidgetManagementMenu($optionsMenu,$widgetRecord)
    {
        if( $widgetRecord['name'] == 'AsdMailChimp' ) {
            $optionsMenu[] = array(
                'title' => __( 'Settings', 'AsdMailChimp', false ),
                'attributes' => array(
                    'class' => '_edit ipsWidgetEdit',
                )
            );
        }
        return $optionsMenu;
    }
}