<?php

namespace Plugin\AsdMailChimp;

class Helper
{

    public static function createForm( $widgetId, $listId ) 
    {
        $form = new \Ip\Form();
        $form->setEnvironment(\Ip\Form::ENVIRONMENT_PUBLIC);
        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'data[listId]',
                'value' => $listId
            )
        );
        $field->addValidator('Required');
        $form->addField( $field );

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'data[email]',
                'label' => __( 'Enter your email to subscribe', 'AsdMailChimp' )
            )
        );

        $field->addValidator('Required');
        $field->addValidator('Email');

        $form->addField( $field );

        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'widgetId',
                'value' => $widgetId
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'sa',
                'value' => 'AsdMailChimp.subscribeEmail'
            ));
        $form->addField($field);

        $form->addField(new \Ip\Form\Field\Submit(array('value' => __( 'Submit', 'AsdMailChimp' ) )));

        return $form;
    }

} 