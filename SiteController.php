<?php
namespace Plugin\AsdMailChimp;

class SiteController extends \Ip\Controller
{
    public function subscribeEmail()
    {
        $postData = ipRequest()->getPost();
        $listId = !empty( $postData['data']['listId'] ) ? $postData['data']['listId'] : '';
        $form = Helper::createForm( null, $listId );

        foreach( $postData['data'] as $field => $value ) {
            $postData["data[{$field}]"] = $value;
        }
        $errors = $form->validate( $postData );

        if ($errors) {
            $status = array('status' => 'error', 'errors' => $errors);
            return new \Ip\Response\Json($status);
        } else {
            $result = Model::call('lists/subscribe', array(
                'id'                => $listId,
                'email'             => array('email'=>$postData['data']['email']),
                //'merge_vars'        => array('FNAME'=>'Davy', 'LNAME'=>'Jones'),
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false,
            ));
            if( $result ) {
                $msg = !empty( $postData['data']['successMessage'] ) ? $postData['data']['successMessage'] : __( 'Email successfully added to mailing list.', 'AsdMailChimp');
                $response = array( 'replaceHtml' => '<div class="msg-success">' . $msg . '</div>' );
            } else {
                $response = array( 'alert' => __( 'Error! Please try again.', 'AsdMailChimp') );
            }
            return new \Ip\Response\Json($response);
        }
    }
}
