<?php

namespace Plugin\AsdMailChimp\Widget\AsdMailChimp;

class Controller extends \Ip\WidgetController
{
    public function getTitle() {
        return __('Mail Chimp', 'AsdMailChimp');
    }

    public function generateHtml( $revisionId, $widgetId, $data, $skin )
    {
        if( empty( $data['serialized'] ) ) {
            $data['serialized'] = '';
        } else {
            parse_str( $data['serialized'], $data );
        }
        if( !empty( $data['listId'] ) ) {
            $data['form'] = \Plugin\AsdMailChimp\Helper::createForm( $widgetId, $data['listId']);
        }
        return parent::generateHtml( $revisionId, $widgetId, $data, $skin );
    }

    public function adminHtmlSnippet()
    {
        $list = array();
        $mailingLists = \Plugin\AsdMailChimp\Model::call('lists/list');
        
        if( $mailingLists ) foreach( $mailingLists['data'] as $mailingList ) {
            $list[] = array( $mailingList['id'], $mailingList['name'] . " ({$mailingList['stats']['member_count']} ".__( 'subscribers', 'AsdMailChimp').")" );
        }

        $form = new \Ip\Form();
        $form->setEnvironment(\Ip\Form::ENVIRONMENT_ADMIN);

        $field = new \Ip\Form\Field\Select(
            array(
                'name' => 'listId',
                'label' => __( 'Choose mailing list', 'AsdMailChimp'),
                'values' => $list
            )
        );
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'data[successMessage]',
                'label' => __( 'Successful subscription message', 'AsdMailChimp' ),
                'note' =>  __( 'Default message is: ', 'AsdMailChimp') . ' ' . __( 'Email successfully added to mailing list.', 'AsdMailChimp')
            )
        );
        $form->addField($field);
        
        return ipView('snippet/edit.php', array( 'form' => $form ))->render();
    }

}
