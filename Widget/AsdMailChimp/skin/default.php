<?php if( !empty( $form ) ): ?>
	<?php echo $form; ?>
<?php elseif( ipIsManagementState() ): ?>
	<div class="text-center">
		<?php echo __( 'There is no mailing list selected.', 'AsdMailChimp'); ?>
	</div>
<?php endif; ?>